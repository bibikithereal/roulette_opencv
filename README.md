What is this?
============
This is an OpenCV/Python project that looks at a roulette table and detects two events: 1) when a game starts, and 2) what the result is.

Assumptions
===========
There is a set of assumptions that need to be configured for this program to work. Namely, you need to be able to tell the program where the center of roulette wheel is. Then, it needs to know the radii of two different ellipses. These ellipses should have same center as roulette wheel and should delimit the region of the wheel where the ball stops. Otherwise, application does not see the ball.
To do this calibration, you should first uncomment these two lines in reading_video.py file:

```
cv2.ellipse(frame, center, (w_outter, h_outter), 0, 0, 360, (0, 0, 255), thickness=1)
cv2.ellipse(frame, center, (w_inner, h_inner), 0, 0, 360, (255, 0, 0), thickness=1)
```

Then, you can use keyboard to move the center and change width and height of the two ellipses.
To move the center use r for right, l for left, u for up, and d for down.
To change outter ellipse's vertical radius, use i (to increase it) and o (to decrease it). To change outter ellipse's horizontal radius, use o (to increase it) and p (to decrease it).
To change inner ellipse's verictal radius, use the dimensions from outter eclipse, and subtract 15 from each. If you feel this is not very precise and you want to get really really smart, go ahead measure the ratios and see how much you should subtract from vertical radius for each pixes that you subtracted in horizontal radius. :)
Once you feel that you have your dimensions right, then hard code them in angle_based_solution.py file because you will need these dimensions every time you run the program.

How does it detect when a game starts?
=====================================
It keeps track of the number zero and compares the angle between two consequtive positions of it. It tracks zero because its color is unique - unique inside roulette wheel.
So, if the angle between two consequtive positions of zero is less than 180 degrees, I put a 1 in the array spin_direction. Now, for many various reasons, this logic may fail. However, it will not fail, for example, 10 times in a row. So, if I have twenty elements in spin_direction array and wheel keeps moving in one direction then sum(spin_direction) will be greater than zero, letting me know with good accuracy that wheel has been moving in one direction. If wheel starts to move in -1 direction, sum(spin_direction) will soon be less than 0, indicating that wheel has been moving in -1 direction. In the meanwhile, I keep track of current_direction. Then, when I calculate last_direction = sum(spin_direction)/abs(sum(spin_direction)), I compare last_direction != current_direction. If this evaluates to true, then it means that wheel changed the direction, which signifies the beginning of a new round.

How does it detect the number?
==============================
It check the position of the ball, and it checks the position of zero. It then evaluates the angle between these two points. It is important to note that this always returns a number between 0 and 359, inclusive. Another fact of importance is that the roulette wheel has 37 numbers in it. This means that each number occupies 360/37 degrees. So, with angle alpha between zero and ball, and with the previous fact, we can detect how far the ball is from zero with 360/(360/37). Based on this value, we can get the index of the number where the ball is inside the array roulette_ = [0, 32, 15, 19...]
Now, this number is not immediately reported because there is a chance that every once in a while, it is read mistakenly. So, instead, the number is put in an array, last_numbers_read. This array will be constantly held at a finite length, for example 50. Then, code checks if this array holds the same number in all of its indices. If yes, then we have the result we need to report.

How does it know not to report result multiple times?
=====================================================
There is a flag, has_been_reported. When a result is reported, this is flipped to true. When code reports that new game started, this flag is flipped to false, so the next time a result is detected, it will be reported.


What's up with running_video.py file?
=====================================
This has two purposes: 1) you can use it to determine the coordinates of the roulette wheel and the dimensions of your ellipses, and 2) you can use it as an alternative way to read the results in roulette based on ocr. However, with the video in this code base, ocr does not work very well. If you would like to see it in action, then run:
If you want to use this file for figuring out appropriate coordinates for roulette wheel's center and ellipses' dimensions, then you should cancel the rotation by commenting out the two lines that invoke the rotate_image() function

```python readin_video.py video.mkv```

Recommendation
==============
Use angle_based_solution.py in production. To run it, execute the following command:

```python angle_based_solution.py [video.mkv|0]```

0 tells program to use camera that is connected in the computer where the program is running.
