import argparse
from functions import *
import cv2

roulette = [0, 32, 15, 19, 4, 21, 2, 25, 17, 34, 6, 27, 13, 36, 11, 30, 8, 23, 10, 5, 24, 16, 33, 1, 20, 14, 31, 9, 22, 18, 29, 7, 28, 12, 35, 3, 26]
last_zero_position = None
direction = 0

img_array = []


parser = argparse.ArgumentParser()
parser.add_argument("video_path", help="path to the video file")

args = parser.parse_args()

capture = cv2.VideoCapture(args.video_path)

if capture.isOpened() is False:
    print("Error opening the video file")

center = (680, 365)
w_outter = 315
h_outter = 190
w_inner = 300
h_inner = 175
step = 5

last_numbers_read_size = 50
last_numbers_read = list(range(last_numbers_read_size))

has_been_reported = False
spin_direction = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
last_direction = 1

while capture.isOpened():
    ret, frame = capture.read()
    if ret is True:
        zeros_contour = get_zeros_contour(frame, (w_outter, h_outter), center)
        balls_contour = get_balls_contour(frame, (w_inner, h_inner), (w_outter, h_outter), center)

        if zeros_contour is not None:
            """we want zeros_centroid updated even if balls_contour is None because we need zeros_centroid for the part of code that checks for game start"""
            zeroes_centroid = centroid(cv2.moments(zeros_contour, True))
            if balls_contour is not None:
                balls_centroid = centroid(cv2.moments(balls_contour, True))
                zeros_centroid_normalized = normalize(zeroes_centroid, w_outter, h_outter, center)
                balls_centroid_normalized = normalize(balls_centroid, w_outter, h_outter, center)

                angle = get_angle(zeros_centroid_normalized, balls_centroid_normalized, (0, 0))
                index = get_number_index(angle)
                number = roulette[index]
                last_numbers_read.append(number)
                if not has_been_reported and all_numbers_same(last_numbers_read, last_numbers_read_size):
                    print("sending result", number, index)
                    has_been_reported = True
                """we keep 50 numbers in this array. if last 50 times we had the same number, then that must be the result."""
                last_numbers_read = last_numbers_read[1:]

                cv2.putText(frame, str(roulette[get_number_index(angle)]), (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA, False)
                cv2.drawContours(frame, [zeros_contour, balls_contour], -1, (255, 0, 255), -10)

        if zeros_contour is not None and  last_zero_position is not None:
            last_zeros_centroid_normalized = normalize(last_zero_position, w_outter, h_outter, center)
            zeros_centroid_normalized = normalize(zeroes_centroid, w_outter, h_outter, center)
            angle = get_angle(last_zeros_centroid_normalized, zeros_centroid_normalized, (0, 0))
            if angle < 180:
                spin_direction.append(1)
            if angle > 180:
                spin_direction.append(-1)

            sum_direction = sum(spin_direction)
            current_direction = sum_direction/abs(sum_direction)
            if has_been_reported and last_direction != current_direction:
                print("NEW GAME STARTED", angle, spin_direction)
                has_been_reported = False
                last_direction = current_direction
            spin_direction = spin_direction[1:]
        last_zero_position = zeroes_centroid


        cv2.imshow('Original frame from the video file', frame)
#        img_array.append(frame)
        if cv2.waitKey(20) & 0xFF == ord('q'):
            height, width, layers = frame.shape
            size = (width,height)
            out = cv2.VideoWriter('project.avi',cv2.VideoWriter_fourcc(*'DIVX'), 15, size)
#            for i in range(len(img_array)):
#                print(i)
#                out.write(img_array[i])
#            out.release()
            break
    else:
        break



capture.release()
cv2.destroyAllWindows()
