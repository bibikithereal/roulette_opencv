import pytesseract
import argparse
import cv2
from functions import *

parser = argparse.ArgumentParser()
parser.add_argument("video_path", help="path to the video file")

args = parser.parse_args()

capture = cv2.VideoCapture(args.video_path)

if capture.isOpened() is False:
    print("Error opening the video file")

center = (680, 365)
w_outter = 315
h_outter = 190
w_inner = 300
h_inner = 175
step = 5

while capture.isOpened():
    ret, frame = capture.read()

    if ret is True:
        unsharped = unsharped_filter(frame)
        balls_contour = get_balls_contour(frame, (w_inner, h_inner), (w_outter, h_outter), center)
        cv2.ellipse(frame, center, (w_outter, h_outter), 0, 0, 360, (0, 0, 255), thickness=1)
        cv2.ellipse(frame, center, (w_inner, h_inner), 0, 0, 360, (255, 0, 0), thickness=1)
        if balls_contour is not None:
            balls_centroid = centroid(cv2.moments(balls_contour, True))
            angle = get_angle(centroid(cv2.moments(balls_contour, True)), (680, 10), center)
            frame = rotate_image(frame, -angle, center)
            rotated = rotate_image(unsharped, -angle, center)
            crop_img = rotated[0:200, 640:700]
            crop_img_gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
            thresh_number, number = cv2.threshold(crop_img_gray, 200, 253, cv2.THRESH_BINARY_INV)
            cv2.imshow("original", frame)
            cv2.imshow("cropped", number)

#        cv2.drawContours(frame, cnts, 0, (255, 0, 255), 10)
#        cv2.rectangle(frame, (640, 0), (700, 200), (0, 255, 0), 1)

        if cv2.waitKey(20) & 0xFF == ord('q'):
            break
        if cv2.waitKey(20) & 0xFF == ord('r'):
            center = (center[0] + step, center[1])
        if cv2.waitKey(20) & 0xFF == ord('l'):
            center = (center[0] - step, center[1])
        if cv2.waitKey(20) & 0xFF == ord('u'):
            center = (center[0], center[1] - step)
        if cv2.waitKey(20) & 0xFF == ord('d'):
            center = (center[0], center[1] + step)
        if cv2.waitKey(20) & 0xFF == ord('o'):
            w_outter += step
        if cv2.waitKey(20) & 0xFF == ord('p'):
            w_outter -= step
        if cv2.waitKey(20) & 0xFF == ord('i'):
            h_outter += step
        if cv2.waitKey(20) & 0xFF == ord('k'):
            h_outter -= step
    else:
        break



capture.release()
cv2.destroyAllWindows()
