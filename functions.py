import time
from math import pi
from math import sin
from math import sqrt
from math import acos
from math import atan2
import pytesseract
import argparse
import cv2
import numpy as np

def unsharped_filter(img):
    """The unsharp filter enhances edges subtracting the smoothed image from the original image"""
    smoothed = cv2.GaussianBlur(img, (9, 9), 10)
    return cv2.addWeighted(img, 1.5, smoothed, -0.5, 0)

def centroid(moments):
    """Returns centroid based on moments"""
    x_centroid = round(moments['m10'] / moments['m00'])
    y_centroid = round(moments['m01'] / moments['m00'])
    return x_centroid, y_centroid

def get_angle(p1, p2, center):
    p1 = np.subtract(p1, center)
    p2 = np.subtract(p2, center)
    angle = atan2(p1[0], p1[1]) - atan2(p2[0], p2[1])
    if angle < 0:
        angle += 2*pi
    return round(180*angle/pi)%360

def get_sign(p1, p2, center):
    p1 = np.subtract(p1, center)
    p2 = np.subtract(p2, center)
    angle = atan2(p1[0], p1[1]) - atan2(p2[0], p2[1])
    if angle < 0:
        return -1
    return 1

def is_inside_zone(p, inner, outter, center):
    """returns true if p is outside elipse defined by inner, and inside elipse defined by outter"""
    px = p[0]-center[0]
    py = p[1]-center[1]

    return ((px/inner[0])**2 + (py/inner[1])**2 > 1) and ((px/outter[0])**2 + (py/outter[1])**2 < 1)

def get_zeros_contour(frame, outter, center):
    """this returns the region in an image that is of green color like number zero is in a roulette table"""
    """because of light angles, opencv may see more than one such regions; therefore, I returned cnts[0], to make sure only one is returned"""
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, (30, 25, 25), (70, 255,255))

    imask = mask>0
    green = np.zeros_like(frame, np.uint8)
    green[imask] = frame[imask]
    gray_frame = cv2.cvtColor(green, cv2.COLOR_BGR2GRAY)
    thresh, blackAndWhiteImage = cv2.threshold(gray_frame, 150, 253, cv2.THRESH_BINARY)
    dilate_kernel = np.ones((16, 16), np.uint8)
    erode_kernel = np.ones((3, 3), np.uint8)

    img_erosion = cv2.erode(blackAndWhiteImage, erode_kernel, iterations=4)
    img_dilated = cv2.dilate(img_erosion, dilate_kernel, 4)
    contours, hierarchy = cv2.findContours(img_dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnts = [c for c in contours if is_inside_zone(centroid(cv2.moments(c, True)), outter, (outter[0] + 50, outter[1] + 50), center)]
    if len(cnts) == 1:
        return cnts[0]


def get_balls_contour(frame, inner, outter, center):
    """this returns a region that has white color and has centroid inside one elipse (outter), and outside another, smaller, elipse (inner)"""
    """the filtering works because there should be no white regions in the spots where a ball may stop in a roulette table"""
    unsharped = unsharped_filter(frame)
    gray_frame = cv2.cvtColor(unsharped, cv2.COLOR_BGR2GRAY)
    thresh, blackAndWhiteImage = cv2.threshold(gray_frame, 245, 253, cv2.THRESH_BINARY)
    dilate_kernel = np.ones((16, 16), np.uint8)
    erode_kernel = np.ones((3, 3), np.uint8)

    img_erosion = cv2.erode(blackAndWhiteImage, erode_kernel, iterations=4)
    img_dilated = cv2.dilate(img_erosion, dilate_kernel, 4)
    contours, hierarchy = cv2.findContours(img_dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnts = [c for c in contours if is_inside_zone(centroid(cv2.moments(c, True)), inner, outter, center)]
    if len(cnts) > 0:
        return cnts[0]

def get_number_index(angle):
    """there are 37 numbers in a roulette table. that means, each number has 1/37 of 360 degrees for it."""
    """so, if we have an angle between number zero on roulette and the location where the ball is stopped, we should be able to determine"""
    """what number the ball has stopped at"""
    """angle accuracy should be within 1 degree, that is why I use int to round it"""
    """the result of this operation, may be to the left or right of the actual value, so I use round to round up or down, depending on what this division returns"""
    return round(int(angle)/(360.0/37))%37

def rotate_image(image, angle, center):
    """you do not need this for angle_based. however, if you use the other solution with ocr, you will need this"""
    """ocr can not read number that are upside down, or sideways. therefore, we need to find where the ball is, rotate the image to put the ball on top,"""
    """and then read the number. so, this function needs to know what image you want to rotate, for what angle, and if you want to scale it. also, it needs to know"""
    """what is the point around which you want to rotate the image"""
    rot_mat = cv2.getRotationMatrix2D(center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result

def normalize(p, r1, r2, center):
    """this function takes a point (p), an elipse's radii (r1 and r2), where r1 is along x axis, and r2 along y axis, and the center of the elipse."""
    """in return you get the coordinates of point after elipse(r1, r2) has been normalized onto a unit circle"""
    p = (p[0] - center[0], p[1] - center[1])
    return ((p[0]*1.0)/r1, (p[1]*1.0)/r2)

def all_numbers_same(numbers, count):
    if len(numbers) < count:
        return False
    first = numbers[0]
    for number in numbers:
        if first != number:
            return False
    return True
